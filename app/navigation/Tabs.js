import React from 'react';
import { TabNavigator } from 'react-navigation';

import Forecast from '../views/Forecast';
import Current from '../views/CurrentWeather';

export const Tabs = TabNavigator({
	Forecast: {
		screen: Forecast
	},
	Current: {
		screen: Current
	}
}, {
	tabBarPosition: 'bottom',
	tabBarOptions: {
		backgroundColor: '#fff',
		activeTintColor: '#fff',
	  activeBackgroundColor: '#0099dd',
		color: '#ff0000',
	  inactiveTintColor: '#999',
	  labelStyle: {
	    fontSize: 16,
	    padding: 12,
			margin: 0
	  },
		indicatorStyle: {
			backgroundColor: '#fff'
		},
		style: {
			backgroundColor: '#2B76D4'
		}
	}
});