import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const ForecastEntry = ({time, temp, humidity, icon}) => {
	return (
		<View style={styles.container}>
			<View style={styles.card}>
				<Text style={styles.text_title}>{time.substring(0, time.lastIndexOf(":"))}</Text>
				<View>
					<Image
						source={{uri: `https://openweathermap.org/img/w/${icon}.png`}}
						style={{width: 60, height: 60, justifyContent: 'center'}}
					/>
				</View>
				<View style={styles.card_bottom}>
					<Text style={styles.text}><Text style={{fontWeight: '600'}}>Temp: </Text><Text>{Math.round(temp)}˚C</Text></Text>
					<Text style={styles.text}><Text style={{fontWeight: '600'}}>Humidity: </Text><Text>{humidity}%</Text></Text>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 10,
		paddingStart: 10,
		paddingEnd: 0,
		justifyContent: 'space-between',
		backgroundColor: '#fff'
	},
	card: {
		paddingTop: 10,
		borderColor: '#eee',
		borderWidth: 1,
		borderRadius: 3,
		backgroundColor: '#eee',
		width: 100,
		alignItems: 'center'
	},
	card_bottom: {
		width: '100%',
		backgroundColor: '#fff',
		paddingBottom: 6,
		paddingTop: 6,
		borderTopWidth: 1,
		borderTopColor: '#eee',
		borderBottomStartRadius: 3,
		borderBottomEndRadius: 3
	},
	text_title: {
		paddingStart: 0,
		fontSize: 13
	},
	text: {
		paddingStart: 10,
		fontSize: 11
	}
})

export default ForecastEntry;