import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Image, TouchableOpacity, AppState } from 'react-native';

import { database } from '../config/Firebase';
import { WEATHER_API_KEY } from '../config/Constants';
import ForecastEntry from '../components/WeatherComponents';

export default class CurrentWeather extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			appState: AppState.currentState
		}
		
		console.disableYellowBox = true;
	}
	
	componentDidMount() {
		AppState.addEventListener('change', this.handleAppStateChange);
		
		this.getCurrentWeather();
	}
	
	componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
	}
	
	render() {
		if (this.state.isLoading) {
			return (
				<View style={{flex: 1, justifyContent: 'center'}}>
					<Text style={{padding: 10, textAlign: 'center'}}>Loading weather...</Text>
					<ActivityIndicator/>
				</View>
			);
		}
		
		if (this.state.isSendingFeedback) {
			return (
				<View style={styles.container}>
					<Text style={styles.city_title}>{this.state.city}, {this.state.country}</Text>
					<View style={styles.container_weather}>
						<View style={{flex: 1, alignItems: 'center'}}>
							<Image
								source={{uri: `https://openweathermap.org/img/w/${this.state.weather.icon}.png`}}
								style={{width: 100, height: 100, justifyContent: 'center'}}
							/>
							<Text style={{color: '#333', fontWeight: '600'}}>Current conditions:</Text>
							<Text style={{color: '#333', paddingTop: 6}}>{this.state.weather.main}</Text>
						</View>
						<View style={{flex: 1, alignItems: 'center'}}>
							<Text style={{color: '#333', fontWeight: '200', fontSize: 48, paddingTop: 40}}>{Math.round(this.state.weatherStats.temp)}˚C</Text>
						</View>
					</View>
				</View>
			);
		} else if (!this.state.isSendingFeedback && this.state.didSendFeedback) {
			return (
				<View style={styles.container}>
					<Text style={styles.city_title}>{this.state.city}, {this.state.country}</Text>
					<View style={styles.container_weather}>
						<View style={{flex: 1, alignItems: 'center'}}>
							<Image
								source={{uri: `https://openweathermap.org/img/w/${this.state.weather.icon}.png`}}
								style={{width: 100, height: 100, justifyContent: 'center'}}
							/>
							<Text style={{color: '#333', fontWeight: '600'}}>Current conditions:</Text>
							<Text style={{color: '#333', paddingTop: 6}}>{this.state.weather.main}</Text>
						</View>
						<View style={{flex: 1, alignItems: 'center'}}>
							<Text style={{color: '#333', fontWeight: '200', fontSize: 48, paddingTop: 40}}>{Math.round(this.state.weatherStats.temp)}˚C</Text>
						</View>
					</View>
								
					<View style={styles.container_feedback}>
						<Text style={{fontWeight: '200', fontSize: 22, paddingTop: 10}}>Ratings</Text>
						<View style={{width: 160, flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>
							<View style={{alignItems: 'center'}}>
								<Image
									source={require('../images/happy.png')}
									style={{width: 40, height: 40, justifyContent: 'center', tintColor: '#00aa00', resizeMode: 'contain'}}
								/>
								<Text style={{fontSize: 20, paddingTop: 4}}>{this.state.votes.up_votes}</Text>
							</View>
							<View style={{alignItems: 'center'}}>									
								<Image
									source={require('../images/sad.png')}
									style={{width: 40, height: 40, justifyContent: 'center', tintColor: '#aa0000', resizeMode: 'contain'}}
								/>
								<Text style={{fontSize: 20, paddingTop: 4}}>{this.state.votes.down_votes}</Text>
							</View>
						</View>
					</View>
				</View>				
			);
		} else {
			return (
				<View style={styles.container}>
					<Text style={styles.city_title}>{this.state.city}, {this.state.country}</Text>
					<View style={styles.container_weather}>
						<View style={{flex: 1, alignItems: 'center'}}>
							<Image
								source={{uri: `https://openweathermap.org/img/w/${this.state.weather.icon}.png`}}
								style={{width: 100, height: 100, justifyContent: 'center'}}
							/>
							<Text style={{color: '#333', fontWeight: '600'}}>Current conditions:</Text>
							<Text style={{color: '#333', paddingTop: 6}}>{this.state.weather.main}</Text>
						</View>
						<View style={{flex: 1, alignItems: 'center'}}>
							<Text style={{color: '#333', fontWeight: '200', fontSize: 48, paddingTop: 40}}>{Math.round(this.state.weatherStats.temp)}˚C</Text>
						</View>
					</View>
								
					<View style={styles.container_feedback}>
						<Text style={{fontWeight: '200', fontSize: 22, paddingTop: 10}}>Is this accurate?</Text>
						<View style={{width: 160, flexDirection: 'row', justifyContent: 'space-between', padding: 20}}>
							<TouchableOpacity onPress={() => {this.updateWeatherFeedbackForCity(this.state.cityId, this.state.city, this.state.date, true)}}>
								<Image
									source={require('../images/thumbs_up.png')}
									style={{width: 40, height: 40, justifyContent: 'center', tintColor: '#00aa00', resizeMode: 'contain'}}
								/>									
							</TouchableOpacity>
							<TouchableOpacity onPress={() => {this.updateWeatherFeedbackForCity(this.state.cityId, this.state.city, this.state.date, false)}}>
								<Image
									source={require('../images/thumbs_down.png')}
									style={{width: 40, height: 40, justifyContent: 'center', tintColor: '#aa0000', resizeMode: 'contain'}}
								/>
							</TouchableOpacity>
						</View>
					</View>
				</View>				
			);
		}
	}
	
	handleAppStateChange = (nextAppState) => {		
		if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
			this.setState({isLoading: true, didSendFeedback: false, appState: nextAppState});	
			this.getCurrentWeather();
		} else {
			this.setState({appState: nextAppState});	
		}
	}
	
	getCurrentWeather() {
		navigator.geolocation.getCurrentPosition(
			position => {
		    this.fetchCurrentWeatherForLocation(position.coords.latitude, position.coords.longitude);
		  },
		  error => {
		  	this.setState({
		    	error: 'Couldn\'t fetch current weather. Check your network connection and try again.'
				});
			}
		);
	}
	
	updateWeatherFeedbackForCity(cityId, cityName, date, isAccurate) {
		let citiesRef = database.ref().child(`cities/${cityId}/${date}`);
		
		this.setState({
			isSendingFeedback: true
		});
		
		citiesRef.once('value')
			.then((snapshot) => {
				if (!snapshot.exists()) {
					let upVotes = isAccurate ? 1 : 0;
					let downVotes = !isAccurate ? 1 : 0;					
					return {
						up_votes: upVotes,
						down_votes: downVotes,
						city_name: cityName
					};
				}
				
				let upVotes = snapshot.val().up_votes;
				let downVotes = snapshot.val().down_votes;
				
				if (isAccurate) {
					upVotes = upVotes + 1;
				} else {
					downVotes = downVotes + 1;
				}
				
				return {
					up_votes: upVotes,
					down_votes: downVotes,
					city_name: cityName					
				};				
			})
			.then((votes) => {				
				citiesRef.set(votes)
					.then(() => {
						this.setState({
							isSendingFeedback: false,
							didSendFeedback: true,
							votes: votes
						});
					})
			})
	}
	
	fetchCurrentWeatherForLocation(latitude, longitude) {
		fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&mode=json&units=metric&APPID=${WEATHER_API_KEY}`)
			.then((response) => response.json())
			.then((responseJson) => {
				console.log("Current weather:", responseJson);
				this.setState({
					isLoading: false,
					weather: responseJson.weather[0],
					weatherStats: responseJson.main,
					cityId: responseJson.id,
					city: responseJson.name,
					country: responseJson.sys.country,
					date: responseJson.dt
				});
			})
			.catch((error) => {
				console.error(error);
			})
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 30
	},
	container_weather: {
		flex: 0.4,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingTop: 20
	},
	container_feedback: {
		flex: 0.275,
		paddingTop: 20,
		alignItems: 'center',
		borderColor: '#eee',
		borderWidth: 1,
		borderRadius: 3,
	},
	button_feedback: {
    backgroundColor: '#859a9b',
    borderRadius: 20,
    padding: 0,
    shadowColor: '#303838',
    shadowOffset: { width: 0, height: 5 },
    shadowRadius: 10,
    shadowOpacity: 0.35,
  },
	city_title: {
		fontSize: 24,
		color: '#000',
		textAlign: 'center'
	}
});