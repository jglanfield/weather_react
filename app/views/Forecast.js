import React from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator, ScrollView, AppState } from 'react-native';

import { WEATHER_API_KEY } from '../config/Constants';
import ForecastEntry from '../components/WeatherComponents';

export default class Forecast extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			appState: AppState.currentState
		}
		
		console.disableYellowBox = true;
	}
	
	componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
		
		this.getForecast();
	}
	
	componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
	}
	
	render() {
		if (this.state.isLoading) {
			return (
				<View style={{flex: 1, justifyContent: 'center'}}>
					<Text style={{padding: 10, textAlign: 'center'}}>Loading forecast...</Text>
					<ActivityIndicator/>
				</View>
			);
		}
		
		console.log(this.state.forecast);

		let forecast = this.state.forecast;
		let forecastDates = [];
		let forecastSections = [];
		for (let key in forecast) {
			let dateComponents = forecast[key].dt_txt.split(" ");
			let date = dateComponents[0];
			let time = dateComponents[1];
			let keyExists = forecastDates.indexOf(date) >= 0;

			if (keyExists) {
				let forecastEntry = forecastSections.find((elt) => { return elt.title === date; })
				forecastEntry.data.push({
					'key': time,
					'temp': forecast[key].main.temp,
					'humidity': forecast[key].main.humidity,
					'icon': forecast[key].weather[0].icon
				});
			} else {
				forecastDates.push(date);
				let dayData = {
					'key': time,
					'temp': forecast[key].main.temp,
					'humidity': forecast[key].main.humidity,
					'icon': forecast[key].weather[0].icon
				}
				forecastSections.push({ title: date, data: [dayData] })
			}
		}

		console.log(forecastSections);

		return (
			<View style={styles.container}>
				<Text style={styles.city_title}>{this.state.city.name}, {this.state.city.country}</Text>
				<ScrollView>
				{
					forecastSections.map((info, index) => {
						return (
							<View style={styles.container} key={index}>
								<Text style={styles.item}>{info.title}</Text>
								<FlatList style={{paddingBottom: 6}}
									horizontal={true}
									data={info.data}
									renderItem={({item}) => <ForecastEntry time={item.key} temp={item.temp} humidity={item.humidity} icon={item.icon}/>}
									/>
							</View>
						)
					})
				}
				</ScrollView>
			</View>
		);
	}
	
	handleAppStateChange = (nextAppState) => {		
		if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
			this.setState({isLoading: true, appState: nextAppState});	
			this.getForecast();
		} else {
			this.setState({appState: nextAppState});	
		}
	}
	
	getForecast() {
		console.log("getting forecast...");
		navigator.geolocation.getCurrentPosition(
			position => {
		    this.fetchForecastForLocation(position.coords.latitude, position.coords.longitude);
		  },
		  error => {
		  	this.setState({
		    	error: 'Couldn\'t fetch weather. Check your network connection and try again.'
				});
			}
		);
	}
	
	fetchForecastForLocation(latitude, longitude) {
		fetch(`https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&mode=json&units=metric&APPID=${WEATHER_API_KEY}`)
			.then((response) => response.json())
			.then((responseJson) => {
				console.log("City details:");
				console.log(responseJson.city);
				console.log(responseJson.city.name);
				console.log(responseJson.city.id);
			
				console.log("Weather info:");
				console.log(responseJson.list[0].dt_txt);
				this.setState({
					isLoading: false,
					forecast: responseJson.list,
					city: responseJson.city
				});
			})
			.catch((error) => {
				console.error(error);
			})
	}
	
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
		paddingTop: 30,
    backgroundColor: '#fff'
  },
	item: {
		paddingTop: 0,
		paddingStart: 10,
		paddingBottom: 4,		
		fontSize: 16,
		backgroundColor: '#fff'
	},
	city_title: {
		fontSize: 24,
		color: '#000',
		textAlign: 'center'
	}
});